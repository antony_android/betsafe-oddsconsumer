package com.betsafe.db;

import com.betsafe.utils.Logging;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class JavaConnect {

    PreparedStatement statement;
    ResultSet result;
    Connection conn = null;
    static final Logging logger = new Logging();

    public static Connection connectDb() {

        try {
            
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sportsbet", "root", "r00t");

            return conn;

        } catch (Exception e) {
          logger.error(e.getMessage());
        }
        return null;
    }
}