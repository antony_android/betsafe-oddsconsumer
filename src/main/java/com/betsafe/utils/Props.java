package com.betsafe.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * Loads system properties from a file.
 *
 * @author karuri
 */
@SuppressWarnings({"FinalClass", "ClassWithoutLogger"})
public final class Props {

    /**
     * The properties file.
     */
    private static final String PROPS_FILE = "conf/configs.conf";
    /**
     * A list of any errors that occurred while loading the properties.
     */
    private final List<String> loadErrors;
    /**
     * Info log level. Default = INFO.
     */
    private static String infoLogLevel = "INFO";
    /**
     * Error log level. Default = ERROR.
     */
    private static String errorLogLevel = "ERROR";
    /**
     * Fatal log level. Default = FATAL.
     */
    private static String fatalLogLevel = "FATAL";

    /**
     * Error log file name.
     */
    private static String infoLogFile;

    /**
     * Error log file name.
     */
    private static String errorLogFile;
    /**
     * Fatal log file name.
     */
    private static String fatalLogFile;

    private static String checkFileStorageDir = "/apps/java/betsafe/";

    /**
     * Database connection pool name.
     */
    private static String dbPoolName;
    /**
     * Database user name.
     */
    private static String dbUserName;
    /**
     * Database password.
     */
    private static String dbPassword;
    /**
     * Database host.
     */
    private static String dbHost;
    /**
     * Database port.
     */
    private static String dbPort;
    /**
     * Database name.
     */
    private static String dbName;

    /**
     * Maximum database connections.
     */
    private static int maxConnections;
    
    private static String apiHostURL;
    
    private static String apiAccount;
    
    private static String apiPassword;
    
    private static String eventsProgramCode;
    
    private static int bookMarkerId;

    /**
     * Constructor.
     */
    public Props() {
        loadErrors = new ArrayList<>(0);
        loadProperties(PROPS_FILE);
    }

    /**
     * Load system properties.
     *
     * @param propsFile the system properties xml file
     */
    @SuppressWarnings({"UseOfSystemOutOrSystemErr", "unchecked"})
    private void loadProperties(final String propsFile) {
        FileInputStream propsStream = null;
        Properties props;

        try {
            props = new Properties();
            File base = new File(
                    //System.getProperty("user.dir")
                    Props.class.getProtectionDomain().getCodeSource().getLocation().toURI()
            ).getParentFile();

            propsStream = new FileInputStream(new File(base, propsFile));
            props.load(propsStream);

            String error1 = "ERROR: %s is <= 0 or may not have been set";
            String error2 = "ERROR: %s may not have been set";

            // Extract the values from the configuration file                     
            setInfoLogLevel(props.getProperty("InfoLogLevel"));
            if (getInfoLogLevel().isEmpty()) {
                loadErrors.add(String.format(error2, "InfoLogLevel"));
            }

            setErrorLogLevel(props.getProperty("ErrorLogLevel"));
            if (getErrorLogLevel().isEmpty()) {
                loadErrors.add(String.format(error2, "ErrorLogLevel"));
            }

            setFatalLogLevel(props.getProperty("FatalLogLevel"));
            if (getFatalLogLevel().isEmpty()) {
                loadErrors.add(String.format(error2, "FatalLogLevel"));
            }

            setInfoLogFile(props.getProperty("InfoLogFile"));
            if (getInfoLogFile().isEmpty()) {
                loadErrors.add(String.format(error2, "InfoLogFile"));
            }

            setErrorLogFile(props.getProperty("ErrorLogFile"));
            if (getErrorLogFile().isEmpty()) {
                loadErrors.add(String.format(error2, "ErrorLogFile"));
            }

            setFatalLogFile(props.getProperty("FatalLogFile"));
            if (getFatalLogFile().isEmpty()) {
                loadErrors.add(String.format(error2, "FatalLogFile"));
            }

            setCheckFileStorageDir(props.getProperty("FilesDir"));
            if (getCheckFileStorageDir().isEmpty()) {
                loadErrors.add(String.format(error2, "FilesDir"));
            }

            setDbPoolName(props.getProperty("DbPoolName"));
            if (getDbPoolName().isEmpty()) {
                loadErrors.add(String.format(error2, "DbPoolName"));
            }

            setDbName(props.getProperty("DbName"));
            if (getDbName().isEmpty()) {
                loadErrors.add(String.format(error1, "DbName"));
            }

            setDbUserName(props.getProperty("DbUserName"));
            if (getDbUserName().isEmpty()) {
                loadErrors.add(String.format(error2, "DbUserName"));
            }

            setDbPassword(props.getProperty("DbPassword"));
            if (getDbPassword().isEmpty()) {
                loadErrors.add(String.format(error2, "DbPassword"));
            }

            setDbHost(props.getProperty("DbHost"));
            if (getDbHost().isEmpty()) {
                loadErrors.add(String.format(error2, "DbHost"));
            }

            setDbPort(props.getProperty("DbPort"));
            if (getDbPort().isEmpty()) {
                loadErrors.add(String.format(error2, "DbPort"));
            }

            String maxConns = props.getProperty("MaximumConnections");
            if (maxConns.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "MaximumConnections"));
            } else {
                setMaxConnections(Integer.parseInt(maxConns));
                if (getMaxConnections() <= 0) {
                    loadErrors.add(String.format(error1,
                            "MaximumConnections"));
                }
            }
            
            String bookmarkerId = props.getProperty("BookmarkerId");
            if (bookmarkerId.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "BookmarkerId"));
            } else {
                setBookmarkerId(Integer.parseInt(bookmarkerId));
                if (getBookmarkerId() <= 0) {
                    loadErrors.add(String.format(error1,
                            "BookmarkerId"));
                }
            }

            setAPIHostURL(props.getProperty("APIHostURL"));
            if (getAPIHostURL().isEmpty()) {
                loadErrors.add(String.format(error2, "APIHostURL"));
            }
            
            setAPIAccount(props.getProperty("APIAccount"));
            if (getAPIAccount().isEmpty()) {
                loadErrors.add(String.format(error2, "APIAccount"));
            }
            
            setAPIPassword(props.getProperty("APIPassword"));
            if (getAPIPassword().isEmpty()) {
                loadErrors.add(String.format(error2, "APIPassword"));
            }
            
            setEventsProgramCode(props.getProperty("EventsProgramCode"));
            if (getEventsProgramCode().isEmpty()) {
                loadErrors.add(String.format(error2, "EventsProgramCode"));
            }

            propsStream.close();
        } catch (NumberFormatException ne) {
            System.err.println("Exiting. String value found, Integer is "
                    + "required: " + ne.getMessage());

            try {
                if(propsStream != null){
                    propsStream.close();
                }
            } catch (IOException ex) {
                System.err.println("Failed to close the properties file: "
                        + ex.getMessage());
            }

            System.exit(1);
        } catch (FileNotFoundException | URISyntaxException ne) {
            System.err.println("Exiting. Could not find the properties file: "
                    + ne.getMessage());

            try {
                if(propsStream != null){
                    propsStream.close();
                }
            } catch (IOException ex) {
                System.err.println("Failed to close the properties file: "
                        + ex.getMessage());
            }

            System.exit(1);
        } catch (IOException ioe) {
            System.err.println("Exiting. Failed to load system properties: "
                    + ioe.getMessage());

            try {
                if(propsStream != null){
                    propsStream.close();
                }
            } catch (IOException ex) {
                System.err.println("Failed to close the properties file: "
                        + ex.getMessage());
            }

            System.exit(1);
        }
    }

    /**
     * @return the infoLogLevel
     */
    public static String getInfoLogLevel() {
        return infoLogLevel;
    }

    /**
     * @param aInfoLogLevel the infoLogLevel to set
     */
    public static void setInfoLogLevel(String aInfoLogLevel) {
        infoLogLevel = aInfoLogLevel;
    }

    /**
     * @return the errorLogLevel
     */
    public static String getErrorLogLevel() {
        return errorLogLevel;
    }

    /**
     * @param aErrorLogLevel the errorLogLevel to set
     */
    public static void setErrorLogLevel(String aErrorLogLevel) {
        errorLogLevel = aErrorLogLevel;
    }

    /**
     * @return the fatalLogLevel
     */
    public static String getFatalLogLevel() {
        return fatalLogLevel;
    }

    /**
     * @param aFatalLogLevel the fatalLogLevel to set
     */
    public static void setFatalLogLevel(String aFatalLogLevel) {
        fatalLogLevel = aFatalLogLevel;
    }

    /**
     * @return the infoLogFile
     */
    public String getInfoLogFile() {
        return infoLogFile;
    }

    /**
     * @param aInfoLogFile the infoLogFile to set
     */
    public static void setInfoLogFile(String aInfoLogFile) {
        infoLogFile = aInfoLogFile;
    }

    /**
     * @return the errorLogFile
     */
    public static String getErrorLogFile() {
        return errorLogFile;
    }

    /**
     * @param aErrorLogFile the errorLogFile to set
     */
    public static void setErrorLogFile(String aErrorLogFile) {
        errorLogFile = aErrorLogFile;
    }

    /**
     * @return the fatalLogFile
     */
    public static String getFatalLogFile() {
        return fatalLogFile;
    }

    /**
     * @param aFatalLogFile the fatalLogFile to set
     */
    public static void setFatalLogFile(String aFatalLogFile) {
        fatalLogFile = aFatalLogFile;
    }

    /**
     * @return the checkFileStorageDir
     */
    public static String getCheckFileStorageDir() {
        return checkFileStorageDir;
    }

    /**
     * @param aCheckFileStorageDir the checkFileStorageDir to set
     */
    public static void setCheckFileStorageDir(String aCheckFileStorageDir) {
        checkFileStorageDir = aCheckFileStorageDir;
    }

    /**
     * @return the dbPoolName
     */
    public static String getDbPoolName() {
        return dbPoolName;
    }

    /**
     * @param aDbPoolName the dbPoolName to set
     */
    public static void setDbPoolName(String aDbPoolName) {
        dbPoolName = aDbPoolName;
    }

    /**
     * @return the dbUserName
     */
    public static String getDbUserName() {
        return dbUserName;
    }

    /**
     * @param aDbUserName the dbUserName to set
     */
    public static void setDbUserName(String aDbUserName) {
        dbUserName = aDbUserName;
    }

    /**
     * @return the dbPassword
     */
    public static String getDbPassword() {
        return dbPassword;
    }

    /**
     * @param aDbPassword the dbPassword to set
     */
    public static void setDbPassword(String aDbPassword) {
        dbPassword = aDbPassword;
    }

    /**
     * @return the dbHost
     */
    public static String getDbHost() {
        return dbHost;
    }

    /**
     * @param aDbHost the dbHost to set
     */
    public static void setDbHost(String aDbHost) {
        dbHost = aDbHost;
    }

    /**
     * @return the dbPort
     */
    public static String getDbPort() {
        return dbPort;
    }

    /**
     * @param aDbPort the dbPort to set
     */
    public static void setDbPort(String aDbPort) {
        dbPort = aDbPort;
    }

    /**
     * @return the dbName
     */
    public static String getDbName() {
        return dbName;
    }

    /**
     * @param aDbName the dbName to set
     */
    public static void setDbName(String aDbName) {
        dbName = aDbName;
    }

    /**
     * @return the maxConnections
     */
    public static int getMaxConnections() {
        return maxConnections;
    }

    /**
     * @param aMaxConnections the maxConnections to set
     */
    public static void setMaxConnections(int aMaxConnections) {
        maxConnections = aMaxConnections;
    }

    /**
     * @return the rabbitHost
     */
    public static String getAPIHostURL() {
        return apiHostURL;
    }

    /**
     * @param apiHost the apiHostURL to set
     */
    public static void setAPIHostURL(String apiHost) {
        apiHostURL = apiHost;
    }

    /**
     * A list of any errors that occurred while loading the properties.
     *
     * @return the loadErrors
     */
    public List<String> getLoadErrors() {
        return Collections.unmodifiableList(loadErrors);
    }

    /**
     * @return the bookMarkerId
     */
    public static int getBookmarkerId() {
        return bookMarkerId;
    }

    /**
     * @param bookMarker the bookMarker to set
     */
    public static void setBookmarkerId(int bookMarker) {
        bookMarkerId = bookMarker;
    }
    
    /**
     * @return the apiAccount
     */
    public static String getAPIAccount() {
        return apiAccount;
    }

    /**
     * @param account the apiAccount to set
     */
    public static void setAPIAccount(String account) {
        apiAccount = account;
    }
    
    /**
     * @return the apiPassword
     */
    public static String getAPIPassword() {
        return apiPassword;
    }

    /**
     * @param password the apiPassword to set
     */
    public static void setAPIPassword(String password) {
        apiPassword = password;
    }
    
    public static String getEventsProgramCode() {
        return eventsProgramCode;
    }

    public static void setEventsProgramCode(String eventsProgramCode) {
        Props.eventsProgramCode = eventsProgramCode;
    }

}
