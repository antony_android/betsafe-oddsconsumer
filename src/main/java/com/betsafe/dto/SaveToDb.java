package com.betsafe.dto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.betsafe.date.LocalDatePersistenceConverter;
import com.betsafe.db.MySQL;
import com.betsafe.utils.Logging;
import com.mysql.cj.jdbc.exceptions.MySQLTransactionRollbackException;
import java.sql.BatchUpdateException;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class SaveToDb {

    private Connection connect;

    public SaveToDb(Connection connect) {
        this.connect = connect;
    }

    PreparedStatement pstmt;
    ResultSet result;
    LocalDatePersistenceConverter converter = new LocalDatePersistenceConverter();
    static final Logging logger = new Logging();

    public int getForeignKey(String table, String Key, String KeyValue) {
        ResultSet result = null;
        int foreignKey = 0;
        String sql = "SELECT * FROM " + table.toLowerCase() + " WHERE " + Key
                + " = '" + KeyValue + "'";

        if (connect == null) {
            connect = MySQL.getConnection();
        }
        PreparedStatement statement2;
        try {
            statement2 = connect.prepareStatement(sql);
            result = statement2.executeQuery();
            while (result.next()) {
                foreignKey = result.getInt(1);

            }
        } catch (Exception e) {
            logger.error("" + e);
        }
        return foreignKey;

    }

    public void InsertSports(String Sport, String SportID, String order) {
        if (connect == null) {
            connect = MySQL.getConnection();
        }
        int priority = 100000;

        if (order != null && order.length() > 0) {

            priority = Integer.parseInt(order);
        }

        try {
            pstmt = connect.prepareStatement(
                    "INSERT INTO sport (`sport_name`,`created_by`, "
                    + "`priority`, `betradar_sport_id`) VALUES (?, ?, ?, ?) "
                    + "ON DUPLICATE KEY UPDATE `priority` = VALUES(priority)");

            pstmt.setString(1, Sport);
            pstmt.setString(2, "IsolutionsAPI");
            pstmt.setInt(3, priority);
            pstmt.setInt(4, Integer.parseInt(SportID));

            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("" + e);
            e.printStackTrace();
        }
    }

    public void InsertGroups(String Group, String GroupID, String Status,
            String SportID, String order) {

        int foreignKey = getForeignKey("Sport", "betradar_sport_id", SportID);
        if (connect == null) {
            connect = MySQL.getConnection();
        }
        
        int priority = 100000;
        if (order != null && order.length() > 0) {

            priority = Integer.parseInt(order);
        }

        try {
            pstmt = connect.prepareStatement(
                    "INSERT INTO category (`category_name`,`status`,"
                    + "`sport_id`,`created_by`,`betradar_category_id`,"
                    + "`priority`) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY "
                    + "UPDATE `priority` = VALUES(priority)");

            pstmt.setString(1, Group);
            pstmt.setInt(2, Integer.parseInt(Status));
            pstmt.setInt(3, foreignKey);
            pstmt.setString(4, "IsolutionsAPI");
            pstmt.setInt(5, Integer.parseInt(GroupID));
            pstmt.setInt(6, priority);
            pstmt.executeUpdate();
        } catch (SQLException e) {

            logger.error("" + e);
            e.printStackTrace();
        }
    }

    public void InsertEvents(String Event, String Group, String EventID,
            String Status, String GroupID, String SportID, String order) {

        int foreignSport = getForeignKey("Sport", "betradar_sport_id", SportID);
        int foreignGroup = getForeignKey("category", "betradar_category_id", GroupID);
        if (connect == null) {
            connect = MySQL.getConnection();
        }

        int priority = 100000;
        if (order != null && order.length() > 0) {

            priority = Integer.parseInt(order);
        }
        
        try {
            pstmt = connect.prepareStatement(
                    "INSERT INTO competition (`competition_name`, "
                    + "`category`,`status`,`category_id`,`sport_id`, "
                    + "`created_by`,`betradar_competition_id`,"
                    + " `priority`,`ussd_priority`) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?,?,?) ON DUPLICATE KEY "
                    + "UPDATE `priority` = VALUES(priority)");
            pstmt.setString(1, Event);
            pstmt.setString(2, Group);
            pstmt.setInt(3, Integer.parseInt(Status));
            pstmt.setInt(4, foreignGroup);
            pstmt.setInt(5, foreignSport);
            pstmt.setString(6, "IsolutionsAPI");
            pstmt.setInt(7, Integer.parseInt(EventID));
            pstmt.setInt(8, priority);
            pstmt.setInt(9, 1);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("" + e);
            e.printStackTrace();
        }
    }

    public void InsertSubEvents(String SubEventID, String homeandwayteam,
            String StartDate, String Status, String EventID, String order) {

        int foreignEvent = getForeignKey("competition",
                "betradar_competition_id", EventID);
        if (connect == null) {
            connect = MySQL.getConnection();
        }
        
        int priority = 100000;
        if (order != null && order.length() > 0) {

            priority = Integer.parseInt(order);
        }

        try {

            if (homeandwayteam.contains("-")) {
                pstmt = connect.prepareStatement(
                        "INSERT INTO `match` (`parent_match_id`,`home_team`, "
                        + "`away_team`,`start_time`,`game_id`, "
                        + "`competition_id`,`status`,`instance_id`, "
                        + "`bet_closure`,`created_by`,`completed`,"
                        + " `priority`) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "
                        + "ON DUPLICATE KEY UPDATE `start_time` = "
                        + "VALUES( start_time), `bet_closure` = "
                        + "VALUES(bet_closure), `priority` = VALUES(priority)");
                pstmt.setInt(1, Integer.parseInt(SubEventID));
                pstmt.setString(2, homeandwayteam.substring(0, homeandwayteam.indexOf("-") - 1));
                pstmt.setString(3, homeandwayteam.substring(homeandwayteam.indexOf("-") + 1));
                pstmt.setTimestamp(4, converter.stringToEntityAttribute(StartDate.replace("T", " ")));
                pstmt.setInt(5, 1);
                pstmt.setInt(6, foreignEvent);
                pstmt.setInt(7, Integer.parseInt(Status));
                pstmt.setInt(8, 1);
                pstmt.setTimestamp(9, converter.stringToEntityAttribute(StartDate.replace("T", " ")));
                pstmt.setString(10, "IsolutionsAPI");
                pstmt.setInt(11, 0);
                pstmt.setInt(12, priority);
                pstmt.executeUpdate();
            }
        } catch (SQLException e) {
            logger.error("" + e);
            e.printStackTrace();
        }
    }

    public String InsertOddType(String OddsClass, String OddsTypeID,
            String OddsClassCode, String SubEventID, String HND, int subTypeId) {

        String query = "INSERT IGNORE INTO odd_type (`name`,`created_by`, "
                + "`active`,`sub_type_id`,`parent_match_id`,"
                + "`live_bet`,`short_name` ,`priority`,"
                + "`special_bet_value`) VALUES('" + OddsClass + "', "
                + "'IsolutionsAPI', '1',  '" + subTypeId + "',"
                + " '" + Integer.parseInt(SubEventID) + "', '0',"
                + " '" + OddsClassCode + "', '0', '" + HND + "')";
        return query;

    }

    public int getSubTypeFromOddClass(String OddsClass) {

        return getForeignKey("sub_type", "subtype_name", OddsClass);
    }

    public void InsertSubType(String OddsClass) {

        if (connect == null) {
            connect = MySQL.getConnection();
        }

        try {

            pstmt = connect.prepareStatement(
                    "INSERT IGNORE INTO sub_type (`subtype_name`) values (?)");

            pstmt.setString(1, OddsClass);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String InsertOdds(String OddsType, String Odds, String HND,
            String Status, String SubEventID, String OddsTypeID,
            String betradarOddsID, int subTypeId) {

        OddsType = OddsType.replaceAll("'", "\'");
        return "INSERT INTO event_odd (`parent_match_id`,`sub_type_id`,"
                + "`max_bet`,`odd_key`, `odd_value`,"
                + "`special_bet_value`,`betradar_odd_id`) VALUES "
                + "('" + Integer.parseInt(SubEventID) + "', '" + subTypeId + "',"
                + " '0.00', '" + OddsType + "', '" + Odds + "', '" + HND + "',"
                + " '" + betradarOddsID + "') ON DUPLICATE KEY UPDATE "
                + "`odd_value` = '" + Odds + "', betradar_odd_id = '" + betradarOddsID + "'";
    }

    public static boolean updateMultiple(ArrayList<String> queries) {
        Connection conn = MySQL.getConnection();
        Statement stmt = null;

        try {
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            for (String query : queries) {

                stmt.addBatch(query);
            }

            int res[] = stmt.executeBatch();
            conn.commit();
            return res.length > 0;
        } catch (MySQLTransactionRollbackException ex) {
            logger.error("MySQLTransactionRollbackException", ex);
            return false;
        } catch (BatchUpdateException ex) {
            logger.error("BatchUpdateException", ex);
            return false;
        } catch (SQLException ex) {
            logger.error("SQLException ", ex);

            return false;
        } catch (Exception ex) {
            logger.error(" Exception", ex);
            return false;
        } finally {

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(SaveToDb.class.getName() + " " + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(SaveToDb.class.getName() + " " + ex.getMessage());
                }
            }
        }
    }

    public static String update(String query) {
        Connection conn = MySQL.getConnection();
        Statement stmt = null;
        ResultSet rs = null;
        String autoIncKey = null;
        //logger.info("Update Query: " + query);

        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);

            rs = stmt.getGeneratedKeys();

            if (rs.next()) {
                autoIncKey = rs.getString(1);
            }
            return autoIncKey;
        } catch (SQLException ex) {
            logger.error(SaveToDb.class.getName() + " Query Failed " + query, ex);
            return null;
        } catch (Exception ex) {
            logger.error(SaveToDb.class.getName() + " Query Failed " + query, ex);
            return null;
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    logger.fatal(SaveToDb.class.getName() + " " + ex.getMessage());
                }
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(SaveToDb.class.getName() + " " + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(SaveToDb.class.getName() + " " + ex.getMessage());
                }
            }
        }
    }

    public static ArrayList<HashMap<String, String>> query(String query) {
        Connection conn = MySQL.getConnection();

        Statement stmt = null;
        ResultSet rs = null;
        ArrayList<HashMap<String, String>> results = new ArrayList<>();

        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next()) {

                ResultSetMetaData metaData = rs.getMetaData();
                String[] columns = new String[metaData.getColumnCount()];
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    columns[i - 1] = metaData.getColumnLabel(i);
                }
                rs.beforeFirst();
                while (rs.next()) {
                    HashMap<String, String> record = new HashMap<>();
                    for (String col : columns) {
                        record.put(col, rs.getString(col));
                    }
                    results.add(record);
                }
            }
            return results;
        } catch (SQLException ex) {
            logger.error(SaveToDb.class.getName() + " Query Failed " + query, ex);
            return results;
        } catch (Exception ex) {
            logger.error(SaveToDb.class.getName() + " Query Failed " + query, ex);
            return results;
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    logger.fatal(SaveToDb.class.getName() + " " + ex.getMessage());
                }
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(SaveToDb.class.getName() + " " + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(SaveToDb.class.getName() + " " + ex.getMessage());
                }
            }
        }
    }
}
