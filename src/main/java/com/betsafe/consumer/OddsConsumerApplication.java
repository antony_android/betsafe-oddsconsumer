package com.betsafe.consumer;

import org.json.simple.JSONObject;

import com.betsafe.consumer.parseodds.ParseOdds;
import com.betsafe.utils.Logging;
import com.betsafe.utils.Props;

public class OddsConsumerApplication {

    static final Logging logger = new Logging();
    static Props props = new Props();

    public static void main(String[] args) {
        try {
            
            ParseOdds odds = new ParseOdds(logger, props);
            JSONObject json = odds.httpGet();
            
            odds.updateOddsLastModified(odds.parseJsonObject(json));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }
}