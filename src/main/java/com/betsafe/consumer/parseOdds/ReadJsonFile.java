package com.betsafe.consumer.parseodds;

import com.betsafe.utils.Logging;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ReadJsonFile {

    static final Logging logger = new Logging();

    public JSONObject readFile() throws Exception, IOException {

        String filepath = "C:/Users/anton/Documents/Office/Betsafe/betsafesoccer.json";
        JSONParser jsonParser = new JSONParser();
        JSONObject json = new JSONObject();
        try (FileReader reader = new FileReader(filepath)) {
            json = (JSONObject) jsonParser.parse(reader);

        } catch (Exception e) {
            logger.error(e.toString());
        }
        return json;
    }
}