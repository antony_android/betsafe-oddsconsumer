package com.betsafe.consumer.parseodds;

import com.betsafe.db.MySQL;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.betsafe.dto.SaveToDb;
import com.betsafe.utils.Logging;
import com.betsafe.utils.Props;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class ParseOdds {

    Connection connect;
    SaveToDb save;
    private static Logging logger;
    private final Props props;

    public ParseOdds(Logging logging, Props props) {

        connect = MySQL.getConnection();
        save = new SaveToDb(connect);
        logger = logging;
        logger.info("Consumer:: Creating Parser for odds");
        this.props = props;
    }

    public JSONObject httpGet() throws Exception {
        JSONObject data_obj = null;
        URL url = new URL(Props.getAPIHostURL() + "/api/eventprogram/GetEventsProgramV6");
        URLConnection con = url.openConnection();
        HttpURLConnection conn = (HttpURLConnection) con;
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);

        String lastModified = "1900-01-01T00:00:00";

        ArrayList<HashMap<String, String>> lastModifiedList = 
                SaveToDb.query("SELECT * FROM odds_last_modified ORDER BY 1 DESC LIMIT 1");

        if (!lastModifiedList.isEmpty()) {
            lastModified = lastModifiedList.get(0).get("last_modified");
            logger.info("Last odds modification date is " + lastModified);
        }

        String outString = "{\"APIAccount\": \"" + Props.getAPIAccount() + "\",\"APIPassword\":\"" + Props.getAPIPassword() + "\",\"EventsProgramCode\":\"" + Props.getEventsProgramCode() + "\",\"LanguageID\": 2,\"DateLastModify\":\"" + lastModified + "\",\"IDBookmaker\":" + Props.getBookmarkerId() + "}";
        logger.info("REQUEST TO API >> " + outString);

        byte[] out = outString.getBytes();
        int length = out.length;

        conn.setFixedLengthStreamingMode(length);
        conn.setRequestProperty("Content-Type", "application/json");

        try (OutputStream os = conn.getOutputStream()) {
            os.write(out);
        }

        conn.connect();
        int responsecode = conn.getResponseCode();
        logger.info("Response code is" + responsecode);
        if (responsecode != 200) {
            throw new RuntimeException("HttpResponseCode: " + responsecode);
        } else {
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            data_obj = stringToJsonObject(response.toString());
        }

        return data_obj;

    }

    public static JSONObject stringToJsonObject(String obj) throws ParseException {

        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(obj);

        return json;
    }

    public String parseJsonObject(JSONObject obj) {

        JSONArray Sports = (JSONArray) obj.get("Sports");
        getSports(Sports);

        try {
            connect.close();
        } catch (SQLException se) {
            logger.error("" + se);
        }
        logger.info("Finished adding prematch odds");
        return (String) obj.get("LastServerDate");
    }
    
    public void updateOddsLastModified(String lastModified){
        
        String query = "INSERT INTO odds_last_modified "
                + "VALUES(NULL, '"+lastModified+"')";
        SaveToDb.update(query);
    }

    @SuppressWarnings("unused")
    public void getSports(JSONArray Sports) {
        JSONObject Sportsobj;

        String SportID = "";
        String Sport = "";
        String Status = "";
        String Order = "";

        int counter = -1;

        for (int i = 0; i < Sports.size(); i++) {
            counter++;
            Sportsobj = (JSONObject) Sports.get(i);
            SportID = Sportsobj.get("SportID").toString();
            Sport = Sportsobj.get("Sport").toString();
            Status = Sportsobj.get("Status").toString();
            Order = Sportsobj.get("Order").toString();
            JSONArray Groups = (JSONArray) Sportsobj.get("Groups");
            save.InsertSports(Sport, SportID, Order);
            getGroups(Groups, SportID);
        }

        logger.info("Sport ID " + SportID + " Sport " + Sport + " Status " + Status + " Order " + Order);

    }

    @SuppressWarnings("unused")
    public void getGroups(JSONArray Groups, String SportID) {
        JSONObject Groupsobj;

        String GroupID = "";
        String Group = "";
        String Status = "";
        String Order = "";
        int counter = -1;

        for (int i = 0; i < Groups.size(); i++) {
            counter++;

            Groupsobj = (JSONObject) Groups.get(i);
            GroupID = Groupsobj.get("GroupID").toString();
            Group = Groupsobj.get("Group").toString();
            Status = Groupsobj.get("Status").toString();
            Order = Groupsobj.get("Order").toString();
            JSONArray Events = (JSONArray) Groupsobj.get("Events");
            save.InsertGroups(Group, GroupID, Status, SportID, Order);
            getEvents(Events, Group, GroupID, SportID);

        }
        logger.info("Group ID " + GroupID + " Group " + Group + " Status " + Status + " Order " + Order);

    }

    @SuppressWarnings("unused")
    public void getEvents(JSONArray Events, String Group, String GroupID, String SportID) {
        JSONObject Eventsobj;
        @SuppressWarnings("rawtypes")
        Iterator objIter = Events.iterator();

        String EventID = "";
        String Event = "";
        String Status = "";
        String Order = "";
        int counter = -1;
        for (int i = 0; i < Events.size(); i++) {
            counter++;
            Eventsobj = (JSONObject) Events.get(i);
            EventID = Eventsobj.get("EventID").toString();
            Event = Eventsobj.get("Event").toString();
            Status = Eventsobj.get("Status").toString();
            Order = Eventsobj.get("Order").toString();
            JSONArray SubEvents = (JSONArray) Eventsobj.get("SubEvents");
            save.InsertEvents(Event, Group, EventID, Status, GroupID,
                    SportID, Order);
            getSubEvents(SubEvents, EventID);
        }

        logger.info("Event ID " + EventID + " Event " + Event + " Status " + Status + " Order " + Order);
    }

    @SuppressWarnings("unused")
    public void getSubEvents(JSONArray SubEvents, String EventID) {
        JSONObject SubEventsobj;

        String SubEventID = "";
        String SubEvent = "";
        String StartDate = "";
        String Status = "";
        String Order = "";

        int counter = -1;
        for (int i = 0; i < SubEvents.size(); i++) {
            counter++;
            SubEventsobj = (JSONObject) SubEvents.get(i);
            SubEventID = SubEventsobj.get("SubEventID").toString();
            SubEvent = SubEventsobj.get("SubEvent").toString();
            StartDate = SubEventsobj.get("StartDate").toString();
            Status = SubEventsobj.get("Status").toString();
            Order = SubEventsobj.get("Order").toString();
            JSONArray Odds = (JSONArray) SubEventsobj.get("Odds");
            save.InsertSubEvents(SubEventID, SubEvent, StartDate, Status,
                    EventID, Order);
            getOdds(Odds, SubEventID);
        }

        logger.info("Matches inserted successfully");

    }

    @SuppressWarnings("unused")
    public void getOdds(JSONArray Odds, String SubEventID) {
        JSONObject Oddsobj;

        String OddsTypeID = "";
        String OddsID = "";
        String OddValue = "";
        String HND = "";
        String OddsClass = "";
        String OddsType = "";
        String StartDate = "";
        String Status = "";
        String Order = "";
        String OddsClassCode = "";
        String betradarOddsID = "";

        ArrayList<String> batch = new ArrayList<>();
        int batchControl = 0;
        int numOfOdds = Odds.size();
        logger.info("Match ID " + SubEventID + " has " + numOfOdds + " Odds");

        for (int i = 0; i < numOfOdds; i++) {
            Oddsobj = (JSONObject) Odds.get(i);
            betradarOddsID = Oddsobj.get("OddsID").toString();
            OddsTypeID = Oddsobj.get("OddsTypeID").toString();
            OddsID = Oddsobj.get("OddsTypeID").toString();
            OddsClass = Oddsobj.get("OddsClass").toString();
            OddsClassCode = Oddsobj.get("OddsClassCode").toString();
            OddsType = Oddsobj.get("OddsType").toString();
            OddValue = Oddsobj.get("Odds").toString();
            HND = Oddsobj.get("HND").toString();
            StartDate = Oddsobj.get("StartDate").toString();
            Status = Oddsobj.get("Status").toString();
            //Order=Oddsobj.get("Order").toString();
            save.InsertSubType(OddsClass);
            int subTypeId = save.getSubTypeFromOddClass(OddsClass);
            if (Integer.parseInt(Status) == 1) {
                batch.add(save.InsertOddType(OddsClass, OddsTypeID, OddsClassCode, SubEventID, HND, subTypeId));
                batch.add(save.InsertOdds(OddsType, OddValue, HND, Status, SubEventID, OddsTypeID, betradarOddsID, subTypeId));
            }
            batchControl++;

            if (batchControl == 300 || i == (numOfOdds - 1)) {

                if (!SaveToDb.updateMultiple(batch)) {
                    logger.error("ERROR IN BATCH WITH THESE QUERIES\n");
                    batch.stream().forEach((query) -> logger.error(query));
                } else {
                    logger.info("Executed batch with " + batchControl + " records successfully");
                }
                batchControl = 0;
            }
        }
    }
}
